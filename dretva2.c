#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

int globalna;

void *dretva() {
  printf("Globalna je: %d\n", globalna);
  sleep(1);
}

int main(void) {
  pthread_t thread_id;

  printf("Prije dretve\n");
  globalna = 10;
  pthread_create(&thread_id, NULL, dretva, NULL);
  pthread_join(thread_id, NULL);
  printf("Nakon dretve\n");

  return 0;
}

