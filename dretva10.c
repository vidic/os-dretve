#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

#define DRETVI 100

int globalna;

void *povecaj(void *povecajZa) {
  int i;
  printf("Povecavam globalnu za: %d\n", *((int *) povecajZa));
  for (i = 0; i < 100000; i++);
  globalna = globalna + *((int *) povecajZa);
  for (i = 0; i < 100000; i++);
}

int main(void) {
  pthread_t dPovecaj[DRETVI];
  const int povecajZa = 1;
  int i;

  globalna = 0;

  printf("Globalna je: %d\n", globalna);
  printf("Prije dretve\n");
  // stvaram dretve
  for (i = 0; i < DRETVI; i++) {
    pthread_create(&dPovecaj[i], NULL, povecaj, (void *) &povecajZa);
  }

  // cekam na zavrsetak svih dretvi
  for (i = 0; i < DRETVI; i++) {
    pthread_join(dPovecaj[i], NULL);
  }
  printf("Nakon dretve\n");
  printf("Globalna je: %d\n", globalna);

  return 0;
}

/*
h00s@jane:/storage/stuff/dev/cdev/vub/thread$ ./a.out 
Globalna je: 0
Prije dretve
Povecavam globalnu za: 1
Povecavam globalnu za: 1
Povecavam globalnu za: 1
Povecavam globalnu za: 1
...
Nakon dretve
Globalna je: 100
h00s@jane:/storage/stuff/dev/cdev/vub/thread$ ./a.out 
Globalna je: 0
Prije dretve
Povecavam globalnu za: 1
Povecavam globalnu za: 1
Povecavam globalnu za: 1
Povecavam globalnu za: 1
...
Nakon dretve
Globalna je: 99
*/
