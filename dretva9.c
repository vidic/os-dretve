#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

void *povecaj(void *povecajZa) {
  printf("Povecat cu za: %d\n", *((int *) povecajZa));
  sleep(1);
}

int main(void) {
  pthread_t dPovecaj[10];
  const int povecajZa = 1;
  int i;

  printf("Prije dretve\n");
  // stvaram dretve
  for (i = 0; i < 10; i++) {
    pthread_create(&dPovecaj[i], NULL, povecaj, (void *) &povecajZa);
  }

  // cekam na zavrsetak svih dretvi
  for (i = 0; i < 10; i++) {
    pthread_join(dPovecaj[i], NULL);
  }
  printf("Nakon dretve\n");

  return 0;
}

/*
h00s@jane:/storage/stuff/dev/cdev/vub/thread$ time ./a.out 
Prije dretve
Ja sam dretva 0
Ja sam dretva 1
Ja sam dretva 2
Ja sam dretva 3
Ja sam dretva 4
Ja sam dretva 7
Ja sam dretva 8
Ja sam dretva 6
Ja sam dretva 9
Ja sam dretva 5
Nakon dretve

real    0m1.001s
user    0m0.000s
sys     0m0.000s
*/