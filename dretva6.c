#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

void *povecaj(void *broj) {
  int* vrijednost = (int*) broj;

  printf("Povecavam vrijednost...\n");
  (*vrijednost)++;
}

int main(void) {
  pthread_t dPrikazi;
  int x;

  printf("Prije dretve\n");
  x = 10;
  pthread_create(&dPrikazi, NULL, povecaj, (void *) &x);
  pthread_join(dPrikazi, NULL);
  printf("Nakon dretve\n");

  printf("Prikazujem x: %d\n", x);

  return 0;
}
