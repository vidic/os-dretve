#include <stdio.h> 
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h> 

#define DRETVI 100

int globalna;
sem_t semafor;

void *povecaj(void *povecajZa) {
  int i;
  printf("Povecavam globalnu za: %d\n", *((int *) povecajZa));
  for (i = 0; i < 100000; i++);
  // cekam na semafor
  sem_wait(&semafor);
  // kriticni odsjecak
  globalna = globalna + *((int *) povecajZa);
  // oslobadjam semafor
  sem_post(&semafor);
  for (i = 0; i < 100000; i++);
}

int main(void) {
  pthread_t dPovecaj[DRETVI];
  const int povecajZa = 1;
  int i;

  globalna = 0;

  printf("Globalna je: %d\n", globalna);
  printf("Prije dretve\n");

  // inicijaliziram semafor
  sem_init(&semafor, 0, 1);

  // stvaram dretve
  for (i = 0; i < DRETVI; i++) {
    pthread_create(&dPovecaj[i], NULL, povecaj, (void *) &povecajZa);
  }

  // cekam na zavrsetak svih dretvi
  for (i = 0; i < DRETVI; i++) {
    pthread_join(dPovecaj[i], NULL);
  }

  // unistavam semafor
  sem_destroy(&semafor);

  printf("Nakon dretve\n");
  printf("Globalna je: %d\n", globalna);

  return 0;
}
